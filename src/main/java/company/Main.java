package company;

public class Main {

    public static void main(String[] args) {

        final int NUMBER_OF_PRODUCERS = 3;
        final int NUMBER_OF_CONSUMERS = 6; 

        MyQueue myQueue = new MyQueue(); // создадим одну общую для всех потоков очередь

        for(int i =0; i <NUMBER_OF_PRODUCERS; i++){
            Thread thread = new Thread(new Producer(myQueue));
            thread.setName("Producer" + (i+1));
            thread.start();
        }

        for(int i=0; i<NUMBER_OF_CONSUMERS; i++){
            Thread thread = new Thread(new Consumer(myQueue));
            thread.setName("Booker" + (i+1));
            thread.start();
        }

    }


}
