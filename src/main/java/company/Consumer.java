package company;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Consumer implements Runnable{

    private MyQueue myQueue;
    private Logger logger = LoggerFactory.getLogger(Consumer.class);
    private final int timeToSleep = 5000;


    public Consumer(MyQueue myQueue){
        this.myQueue = myQueue;
    }

    @Override
    public void run() {


        Request request;
        while((request = myQueue.get()) != null) {
            try {
                logger.info("{} processing...", Thread.currentThread().getName() );
                Thread.sleep(timeToSleep);
                logger.info("{} processed {}", Thread.currentThread().getName(), request);

            } catch (InterruptedException e) {
                logger.error("ThreadInterrupted");
            }
        }


    }
}
