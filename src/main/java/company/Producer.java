package company;




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.Random;

public class Producer implements Runnable{

    private Random rand = new Random();
    private MyQueue myQueue;
    private Logger logger = LoggerFactory.getLogger(Producer.class);




    public Producer(MyQueue myQueue){
        this.myQueue = myQueue;
    }


    @Override
    public void run() {

        while(!Thread.currentThread().isInterrupted()) {

            Request request = new Request(rand.nextInt(), LocalDate.now(), "hotel"); // создаем запрос на бронирование
            try {
                myQueue.put(request); // добавляем его в очередь

            } catch (InterruptedException e) {
                logger.error("ThreadInterrupted");
            }
        }


    }
}
