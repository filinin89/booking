package company;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.Queue;

public class MyQueue {

    private volatile Queue<Request> roomsQueue = new LinkedList<>(); // одна из реализаций очереди - подходит под наши цели // Можно использовать и LinkedList
    private static final int CAPACITY = 5; // емкость очереди
    private volatile int entireCounter;
    private final int maxRequests = 15;
    private Logger logger = LoggerFactory.getLogger(MyQueue.class);

    public int getEntireCounter() {
        return entireCounter;
    }
    public int getMaxRequests() {
        return maxRequests;
    }
    public Queue<Request> getRoomsQueue() {
        return roomsQueue;
    }



    public synchronized Request get() { // как только запрос появился этот поток занимает монитор, потому что раньше его освобождал и объявилл об ожидании

        while(roomsQueue.isEmpty() && entireCounter < maxRequests){ // второе услови нобходимо для завершения программы
            try {
                wait(); // если запросов в очереди нет - ждем. Этот метод освобождает монитор и останавливает выполнение метода get до тех пор пока не будет вызван notify
            } catch (InterruptedException e) {
                logger.error("ThreadInterrupted");
            }
        }
        if(entireCounter >= maxRequests && roomsQueue.isEmpty()){ // этот блок обязательно здесь и обязательно соблюдать последнее условие
            Thread.currentThread().interrupt();
            return null; // здесь нужно явно выйти из метода
        }
        Request result = this.roomsQueue.poll(); // как только запрос появился тут же его достанем // можно разбить на две операции - взять и потом удалить в Consumer
        logger.info("{} received  {}  Очередь: {}", Thread.currentThread().getName(), result, roomsQueue.size());
        notifyAll();

        return result;

    }


    public synchronized void put(Request request) throws InterruptedException {

        while(roomsQueue.size() >= CAPACITY ){ // ограничение очереди по размеру
            wait();                    // ждем пока место освободится
        }
        if(entireCounter >= maxRequests) {
            Thread.currentThread().interrupt();
        }else {
            roomsQueue.offer(request); //  добавим эл в конец очереди
            entireCounter++;
            logger.info("{} sent  {}  Очередь: {}" , Thread.currentThread().getName(), request, roomsQueue.size());
            logger.info("    Общее кол-во сгенерированных запросов " + entireCounter);
            notifyAll(); // как только добавили запрос, значит его можно забирать в get, значит можно оповестить об этом потоки? которые заснули внутри метода get()

        }
    }

}
